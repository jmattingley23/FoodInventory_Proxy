package xyz.jmatt.results;

public class SimpleResult {
    private boolean isError;
    private String message;

    public SimpleResult(String message, boolean isError) {
        this.message = message;
        this.isError = isError;
    }

    public boolean isError() {
        return isError;
    }

    public String getMessage() {
        return message;
    }
}
