package xyz.jmatt;


import com.google.gson.Gson;

import xyz.jmatt.models.ItemModel;
import xyz.jmatt.results.SimpleResult;

public class ServerProxy {
    private ServerCommunicator serverCommunicator;

    private final String PROCESS_ITEM_URL = "/processItem";
    private final String SET_SCAN_STATE_URL = "/setScan";
    private final String GET_SCAN_STATE_URL = "/getScan";
    private final String GET_RECENT_ITEMS_URL = "/getRecentItems";
    private final String SET_CLIENT_TOKEN_URL = "/setClientToken";
    private final String GET_ALL_ITEMS_URL = "/getAllItems";
    private final String UPDATE_ITEM_URL = "/updateItem";

    private static final String SERVER_ERROR_STRING = "ERROR: Cannot connect to server";

    private static final String NULL_ERROR_STRING = "ERROR: null";

    public ServerProxy() {}

    /**
     * Connects the server proxy to the given server on the given port
     * @param host the server host to connect to
     * @param port the port to connect on
     */
    public void connect(String host, String port) {
        System.out.println("Connecting to server...");
        serverCommunicator = new ServerCommunicator(host, port);
        System.out.println("Connected to server [" + host + "] on port [" + port + "].");
    }

    public SimpleResult processItem(String itemCode) {
        SimpleResult result = (SimpleResult)sendPostToServer(PROCESS_ITEM_URL, itemCode, SimpleResult.class);
        if(result == null) {
            result = new SimpleResult(NULL_ERROR_STRING, true);
        }
        return result;
    }

    public SimpleResult setScanState(String state) {
        SimpleResult result = (SimpleResult)sendPostToServer(SET_SCAN_STATE_URL, state, SimpleResult.class);
        if(result == null) {
            result = new SimpleResult(NULL_ERROR_STRING, true);
        }
        return result;
    }

    public SimpleResult getScanState() {
        SimpleResult result = (SimpleResult)sendGetToServer(GET_SCAN_STATE_URL, SimpleResult.class);
        if(result == null) {
            result = new SimpleResult(NULL_ERROR_STRING, true);
        }
        return result;
    }

    public ItemModel getRecentItems() {
        ItemModel result = (ItemModel)sendGetToServer(GET_RECENT_ITEMS_URL, ItemModel.class);
        if(result == null) {
            result = new ItemModel(NULL_ERROR_STRING, true);
        }
        return result;
    }

    public SimpleResult setClientToken(String token) {
        SimpleResult result = (SimpleResult)sendPostToServer(SET_CLIENT_TOKEN_URL, token, SimpleResult.class);
        if(result == null) {
            result = new SimpleResult(NULL_ERROR_STRING, true);
        }
        return result;
    }

    public ItemModel getAllItems() {
        ItemModel result = (ItemModel)sendGetToServer(GET_ALL_ITEMS_URL, ItemModel.class);
        if(result == null) {
            result = new ItemModel(NULL_ERROR_STRING, true);
        }
        return result;
    }

    public SimpleResult updateItemModel(ItemModel model) {
        String JSON = new Gson().toJson(model);
        SimpleResult result = (SimpleResult)sendJSONPostToServer(UPDATE_ITEM_URL, JSON, SimpleResult.class);
        if(result == null) {
            result = new SimpleResult(NULL_ERROR_STRING, true);
        }
        return result;
    }

    private Object sendPostToServer(String URL, String data, Class classType) {
        String result = serverCommunicator.sendSimplePost(URL, data);
        if(result.equals(SERVER_ERROR_STRING)) {
            try {
                return classType.getConstructor(String.class, boolean.class).newInstance(result, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new Gson().fromJson(result, classType);
    }

    private Object sendGetToServer(String URL, Class classType) {
        String result = serverCommunicator.sendSimpleGet(URL);
        if(result.equals(SERVER_ERROR_STRING)) {
            try {
                return classType.getConstructor(String.class, boolean.class).newInstance(result, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new Gson().fromJson(result, classType);
    }

    private Object sendJSONPostToServer(String URL, String JSON, Class classType) {
        String result = serverCommunicator.sendJSONPost(URL, JSON);
        if(result.equals(SERVER_ERROR_STRING)) {
            try {
                return classType.getConstructor(String.class, boolean.class).newInstance(result, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new Gson().fromJson(result, classType);
    }
}
