package xyz.jmatt.models;

public class UPCItem {
    private String title;
    private UPCOffer[] offers;
    private String[] images;

    public String getTitle() {
        return title;
    }

    public UPCOffer[] getOffers() {
        return offers;
    }

    public String[] getImages() {
        return images;
    }
}
