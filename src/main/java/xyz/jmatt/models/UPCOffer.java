package xyz.jmatt.models;

public class UPCOffer {
    private String title;
    private String domain;
    private double price;
    private String link;

    public String getTitle() {
        return title;
    }

    public String getDomain() {
        return domain;
    }

    public double getPrice() {
        return price;
    }

    public String getLink() {
        return link;
    }
}
