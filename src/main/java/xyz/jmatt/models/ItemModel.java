package xyz.jmatt.models;

import java.util.List;

/**
 * Simple container class to hold information about an item
 */
public class ItemModel {
    private String barcode;
    private String formattedBarcode;
    private long lastAdded;
    private String title;
    private String image;
    private long expiration;
    private int quantity;
    private List<ItemModel> items;
    private String message;
    private boolean isError;

    public ItemModel() {
        title = "UNKNOWN ITEM";
        image = null;
        isError = false;
    }

    public ItemModel(String message, boolean isError) {
        this.isError = isError;
        this.message = message;
    }

    //setters
    public void setBarcode(String barcode) {
        this.barcode = barcode;
        formatBarcode();
    }

    public void setLastAdded(long lastAdded) {
        this.lastAdded = lastAdded;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setItems(List<ItemModel> items) {
        this.items = items;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setError(boolean isError) {
        this.isError = isError;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    //getters
    public String getBarcode() {
        return barcode;
    }

    public long getLastAdded() {
        return lastAdded;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public int getQuantity() {
        return quantity;
    }

    public List<ItemModel> getItems() {
        return items;
    }

    public String getMessage() {
        return message;
    }

    public long getExpiration() {
        return expiration;
    }

    public boolean isError() {
        return isError;
    }

    private void formatBarcode() {
        if(barcode.length() == 12) {
            StringBuilder sb = new StringBuilder();
            sb.append(barcode.substring(0, 1));
            sb.append(" ");
            sb.append(barcode.substring(1, 6));
            sb.append(" ");
            sb.append(barcode.substring(6, 11));
            sb.append(" ");
            sb.append(barcode.substring(11, 12));
            formattedBarcode = sb.toString();
        } else {
            formattedBarcode = barcode;
        }
    }

    public String getFormattedBarcode() {
        return formattedBarcode;
    }
}
