package xyz.jmatt.models;

public class UPCModel {
    private String code;
    private int total;
    private UPCItem[] items;

    public String getCode() {
        return code;
    }

    public int getTotal() {
        return total;
    }

    public UPCItem[] getItems() {
        return items;
    }
}
