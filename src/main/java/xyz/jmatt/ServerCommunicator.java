package xyz.jmatt;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerCommunicator {
    private static String serverHost;
    private static String serverPort;
    private static final String SERVER_ERROR_STRING = "ERROR: Cannot connect to server";

    public ServerCommunicator(String host, String port) {
        serverHost = host;
        serverPort = port;
    }

    /**
     * Sends a POST to the given url on the server
     * @param urlSuffix the route on the server to use
     * @param data any data to send with the post
     * @return a JSON representation of the result from the server
     */
    String sendSimplePost(String urlSuffix, String data) {
        try {
            //construct a URL and open the connection
            URL url = new URL("http://" + serverHost + ":" + serverPort + urlSuffix + "/" + data);
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            http.setConnectTimeout(10000);

            http.setDoOutput(true);
            http.setRequestMethod("POST");
            http.connect();

            //get the data from the result and return it as a string
            InputStream respBody = http.getInputStream();
            String respData = readString(respBody);
            return respData;
        } catch (Exception e) {
            e.printStackTrace();
            return SERVER_ERROR_STRING;
        }
    }

    String sendJSONPost(String urlSuffix, String JSON) {
        try {
            URL url = new URL("http://" + serverHost + ":" + serverPort + urlSuffix);
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            http.setConnectTimeout(10000);

            http.setDoOutput(true);
            http.setRequestMethod("POST");
            http.connect();

            OutputStream reqBody = http.getOutputStream();
            writeString(JSON, reqBody);
            reqBody.close();

            InputStream respBody = http.getInputStream();
            String respData = readString(respBody);
            return respData;
        } catch (Exception e) {
            e.printStackTrace();
            return SERVER_ERROR_STRING;
        }
    }

    /**
     * Sends a GET to the server on the given URL
     * @param urlSuffix the route on the server to use
     * @return a JSON representation of the result form the server
     */
    String sendSimpleGet(String urlSuffix) {
        try {
            //construct a URL and open the connection
            URL url = new URL("http://" + serverHost + ":" + serverPort + urlSuffix);
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            http.setConnectTimeout(10000);

            http.setRequestMethod("GET");
            http.setDoOutput(false);
            http.connect();

            //get the data from the result and return it as a string
            InputStream respBody = http.getInputStream();
            String respData = readString(respBody);
            return respData;
        } catch (Exception e) {
            e.printStackTrace();
            return SERVER_ERROR_STRING;
        }
    }

    /**
     * Reads the give InputStream and converts it to a string
     * @param is the inputstream to read from
     * @return a string representation of the text
     * @throws IOException thrown if the Stream could not be converted
     */
    private static String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    /**
     * Writes the given strong to an OutputStream
     * @param str the string to write
     * @param os the outputstream to write to
     * @throws IOException thrown if the string could not be written to the stream
     */
    private static void writeString(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
    }
}
